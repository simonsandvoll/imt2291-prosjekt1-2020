<?php 

require_once 'classes/TwigController.php';
require_once 'classes/Validator.php';
require_once 'classes/DB.php';
require_once 'classes/User.php';
require_once 'classes/Admin.php';
require_once 'classes/Teacher.php';
require_once 'classes/Student.php';
require_once 'classes/Video.php';
require_once 'classes/Playlist.php';

$db = DB::getDBConnection();


if ($db != null) {

  $twig = new TwigController();

  $user = new User($db);

  $valid = null;
  
  if (!file_exists('uploadedFiles/tmp.jpg')) {
    @mkdir("uploadedFiles");
  }

  if (isset($_POST['loginUser'])) {
    $valid = Validator::validateLoginData($_POST['email'], $_POST['password']);
    if ($valid['status'] === 'OK') {
      $loginTest = $user->loginUser($_POST['email'], $_POST['password']);
      if ($loginTest['status'] !== 'OK') {
        $twig->render('error.html', $loginTest);
      }
    } else {
      $twig->render('error.html', $valid);
    }
  } 
  
  if (isset($_POST['createUser'])) {
    $valid = Validator::validateCreateUserData($_POST);
    if ($valid['status'] === 'OK') {
      $createUserTest = $user->addUser($_POST);
      if ($createUserTest['status'] !== 'OK') {
        $twig->render('error.html', $createUserTest);
      }
    } else {
      $twig->render('error.html', $valid);
    }
  }

  if (isset($_POST['logout'])) {
    $user->logout(); 
  }

  if($user->loggedIn()) {
    if ($_SESSION['userData']['userType'] === 'admin') {
      $admin = new Admin($user, $twig);

    } else if ($_SESSION['userData']['userType'] === 'teacher') {
      $video = new Video($db);
      $playlist = new Playlist($db);
      $teacher = new Teacher($twig, $video, $playlist);

    } else if ($_SESSION['userData']['userType'] === 'student') {
      $video = new Video($db);
      $playlist = new Playlist($db);
      $student = new Student($twig, $video, $playlist);
    }
  } else {
    $twig->render('index.html', array());
  }

} else {
  $twig->render('error.html', array('errorMessage' => 'Klarte ikke å koble til databasen!'));
}

?>