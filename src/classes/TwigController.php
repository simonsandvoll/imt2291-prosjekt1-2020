<?php
/**
 * This class is a twig class where the twig environment is. And where the templates
 * are rendered. 
 */

session_start();
require_once '../vendor/autoload.php';

class TwigController {

  public $twig;

  /* On creation of an object, the twig loader and enivorment is created */
  public function __construct() {
    $loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
    $this->twig = new \Twig\Environment($loader, [
        /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
    ]);
  }

  /** 
   * Echos the render function of the twig enviroment
   * @param string twigTemplate to be rendered; like index.html
   * @param array $package, an array of variables needed for displaying
   * data in twig. 
  */
  public function render($filePath, $package) {
    echo $this->twig->render($filePath, $package);
  }

  /**
   * sets a global twig variable, accessed throughout the platform.
   * @param string $key, the name of the global variable; like 'user'.
   * @param var $var a variable being determined for global status. 
  */
  public function setGlobal($key, $var) {
    $this->twig->addGlobal($key, $var);
  }

  /**
   * @param array $statusArr, array to be checked. if status is OK we render
   * index.html, if not we render the error.html template. On an error we render
   * the $statusArr array containing error messages.
   * @param array $viewArr, array being viewed on a status 'OK'.
  */
  public function indexOrError($statusArr, $viewArr) {
    if (isset($viewArr['title'])) {
      $viewArr['view'] = 'confirmation';
    }
    if ($statusArr['status'] === 'OK') {
      $this->render('index.html', $viewArr);
    } else {
      $this->render('error.html', $statusArr);
    }
  }
}
?>