<?php

class DB {
  private static $db=null;
  private $dsn = 'mysql:dbname=lecturevideos;host=db';
  private $user = 'user';
  private $password = 'test';
  private $dbh = null;

  private function __construct() {
    try {
        $this->dbh = new PDO($this->dsn, $this->user, $this->password);
    } catch (PDOException $e) {
        // NOTE IKKE BRUK DETTE I PRODUKSJON
        echo 'Connection failed: ' . $e->getMessage();
    }
  }

  /**
   * Creates a database connection
   * @return object of class database, with the connection to the database.
  */
  public static function getDBConnection() {
      if (DB::$db==null) {
        DB::$db = new self();
      }
      return DB::$db->dbh;
  }
}
