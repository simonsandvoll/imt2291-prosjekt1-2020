<?php

/**
 * This class is for adding, editing, deleting 
 * and searching for playlists in the lectureoverview database.
*/

class Playlist {
  private $db = null;
  public $width = 150;
  public $height = 150;

  /**
   * Connect to the database when object is created.
  */
  public function __construct($db) {
    $this->db = $db;
  }

  /** 
   * Function that prepares the data for insertion, meaning encoding the thumbnail,
   * and all other data about the playlist being inserted/updated. 
   * @param array $formData is all the data from the update/insert post request.
   * @param array $file is the file information from the update/insert post request. 
   * @param string $username. When updating the table we don't need the username,
   * but when inserting we do. 
   * @return array with the data needed for insertion/deletion. "title", "description",
   * "course", "topic", "thumbnail" and "username"/"idToUpdate".
  */
  public function prepareDataForInsertion($formData, $file, $username) {
    $thumbnail = file_get_contents(
      $file['thumbnail']['tmp_name']);
    $scaledThumbnail = $this->scale(imagecreatefromstring(
      $thumbnail), $this->width, $this->height);
    unset($thumbnail);
    $mime = $file['thumbnail']['type'];
    $fileExtension = "png";
    if ($mime === "image/png") {
      $fileExtension = "png";
    } else if ($mime === "image/jpeg") {
      $fileExtension = "jpeg";
    } else if ($mime === "image/jpg") {
      $fileExtension = "jpg";
    }
    $filePath = $file['thumbnail']['tmp_name'];
    $data = ["title" => $formData['title'], "topic" => $formData['topic'], 
    "course" => $formData['course'], "description" => $formData['description'], 
    "thumbnail" => $scaledThumbnail, "mime" => $mime, 
    'filePath' => $filePath, "fileExtension" => $fileExtension];

    $data['username'] = $username;
    if (isset($formData['id'])) {
      $data['playlistId'] = $formData['id'];
    }

    $data['videosToAdd'] = isset($formData['videosToAdd']) ? $formData['videosToAdd'] : null;
    return $data;
  }

  /**
   * Adds a playlist to the database.
   * @param array with 'username', 'title', 'course', 'topic', 
   * 'description' and 'thumbnail'.
   * If the $data includes any vides to add to the playlist this function
   * sends that data to the addVideosToPlaylist function of this class.
   * @return an array with only element 'status'=='OK' on success.
   *        'status'=='FAIL' on error, the error info can be found
   *        in 'errorInfo'.
  */
  public function createPlaylist($data) {
    $playlistId = uniqid();

    $sql = "INSERT INTO Playlist 
      (id, userId, title, course, topic, description, 
        thumbnail, mime)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    
    $sth = $this->db->prepare($sql);

    $sqlData = array($playlistId, $data['username'], 
      $data['title'], $data['course'], $data['topic'], 
      $data['description'], $data['thumbnail'], $data['mime']);

    $sth->execute($sqlData);

    $tmp = [];

    if ($sth->rowCount()==1) {
      if ($data['thumbnail'] != '') {

        $username = $data['username'];
        $filePath = $data['filePath'];
        $fileExtension = $data['fileExtension'];

        if(!is_dir("uploadedFiles/$username")) {
          @mkdir("uploadedFiles/$username");
        }
        if(!is_dir("uploadedFiles/$username/$playlistId")) {
          @mkdir("uploadedFiles/$username/$playlistId");
        }
        $thumbnailDestination = "uploadedFiles/$username/$playlistId/thumbnail.$fileExtension";
        if (!file_exists($thumbnailDestination)) {
          if(@move_uploaded_file($filePath, $thumbnailDestination)) {
            $tmp['status'] = 'OK';
          } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Could not upload file';
          }
        } else {
          $this->deleteDirectoryOrFile($thumbnailDestination);
          if(@move_uploaded_file($filePath, $thumbnailDestination)) {
            $tmp['status'] = 'OK';
          } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Could not upload file';
          }
        }
      }
      $tmp['id'] = $playlistId;
      if ($data['videosToAdd'] != null) {
        $valid = $this->addVideosToPlaylist($playlistId, $data['videosToAdd']);
        if ($valid['status'] === 'OK') {
          $valid['message'] = 'Videoer lagt til';
        }
        $valid['id'] = $playlistId;
        return $valid;
      }
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fikk ikke lagd spilleliste!';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }

    return $tmp;
  }

  /**
   * Adds a video to the VideoInPlaylist table in the database.
   * @param array with 'videoId', 'playlistId', 'rank', 
   * @param string $playlistId, id of the playlist that is getting videos.
   * @param array $videos with videoId and rank (position) of the videos.
   * @return an array with only element 'status'=='OK' on success.
   *        'status'=='FAIL' on error, the error info can be found
   *        in 'errorInfo'.
  */
  private function addVideosToPlaylist($playlistId, $videos) {
    $deleteQuery = "DELETE FROM VideosInPlaylist WHERE playlistId=?";
    $deleteSth = $this->db->prepare($deleteQuery);
    $deleteData = array($playlistId);
    $deleteSth->execute($deleteData);

    $tmp = array();
    $tmp['errorInfo'] = array();
    $sqlStr = "INSERT INTO VideosInPlaylist 
    (videoId, playlistId, rank)
    VALUES";
    $values = '';
    $size = sizeof($videos) -1;
  
    foreach($videos as $key => $value){
      if ($key === $size) {
        $values .= " (?, ?, ?);";
      } else {
        $values .= " (?, ?, ?), ";
      }
    }
    $sqlStr .= $values;
    $sqlData = array();

    $sth = $this->db->prepare($sqlStr);

    for ($x=0; $x<count($videos); $x++) {
      array_push($sqlData, $videos[$x]['videoId'], $playlistId, $videos[$x]['rank']);
    }
    print_r($sqlData);
    $sth->execute($sqlData);
    if ($sth->rowCount()==0) {
      array_push($tmp['errorInfo'], 'Fikk ikke lagd videoer til spillelisten!');
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      array_push($tmp['errorInfo'], $this->db->errorInfo()[2]);
    }
    $tmp['status'] = (count($tmp['errorInfo']) === 0 ? 'OK' : 'FAIL');
    return $tmp;

  }

  /**
   * Updates a playlist in the database.
   * @param array with 'idToUpdate', 'title', 
   * 'course', 'topic', 'description' and 'thumbnail'.
   * @param array or videos being added/reordered in the playlist
   * with videoId and rank (position) of the video in the playlist order. 
   * @return an array with only element 'status'=='OK' on success.
   *        'status'=='FAIL' on error, the error info can be found
   *        in 'errorInfo'.
  */
  public function updatePlaylist($playlist, $videos) {
    $idToUpdate = $playlist['playlistId'];

    $sql = "UPDATE Playlist SET
      title=?, course=?, topic=?, 
      description=?, thumbnail=?, mime=?
      WHERE id=?";
    
    $sth = $this->db->prepare($sql);

    $sqlData = array($playlist['title'], $playlist['course'],
      $playlist['topic'], $playlist['description'],
      $playlist['thumbnail'], $playlist['mime'], $idToUpdate);
      
    $sth->execute($sqlData);

    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      if ($playlist['thumbnail'] != '') {
        $username = $playlist['username'];
        $filePath = $playlist['filePath'];
        $fileExtension = $playlist['fileExtension'];
        if(!is_dir("uploadedFiles/$username/$idToUpdate")) {
          @mkdir("uploadedFiles/$username/$idToUpdate");
        }
        $thumbnailPath = "uploadedFiles/$username/$idToUpdate/thumbnail.$fileExtension";
        if (!file_exists($thumbnailPath)) {
          if(@move_uploaded_file($filePath, $thumbnailPath)) {
            $tmp['status'] = 'OK';
          } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Fikk ikke lastet opp bildet!';
          }
        } else {
          $this->deleteDirectoryOrFile($thumbnailPath);
          if(@move_uploaded_file($filePath, $thumbnailPath)) {
            $tmp['status'] = 'OK';
          } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Fikk ikke lastet opp bildet!';
          }
        }
      }
      $videosInPlaylist = $this->addVideosToPlaylist($idToUpdate, $videos);

      if ($videosInPlaylist['status'] === 'OK') {
        $tmp['status'] = 'OK';
      } else {
        $tmp['status'] = 'FAIL';
        $tmp['errorMessage'] = $videosInPlaylist['errorMessage'];
      }
/*       $videoSql = "INSERT INTO 
         VideosInPlaylist(videoId, rank, playlistId) 
         VALUES (?, ?, ?) ON DUPLICATE KEY 
         UPDATE videoId=?, rank=? WHERE playlistId=?";
      $videoSql = "UPDATE  VideosInPlaylist SET rank=? WHERE playlistId=? AND videoId=?;"; 
      for ($y=0; $y<$nrOfVideos; $y++) {
        $videoSth = $this->db->prepare($videoSql);
        $videoSqlData = array($videos[$y]['videoId'], $videos[$y]['rank'], $idToUpdate);
        $videoSth->execute($videoSqlData);
      }*/
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fikk ikke oppdatert spillelisten!';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }
    return $tmp;
  }

  /**
   * Deletes the playlist with the given ID from the database.
   *
   * @param  String $id the id of the playlist to be deleted
   * @return Array with the elements status=OK if success, else status=FAIL
  */
  public function deletePlaylist($id, $username) {
    $sql = 'DELETE FROM Playlist WHERE id=?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    if ($sth->rowCount() == 1) {
      $this->deleteDirectoryOrFile("uploadedFiles/$username/$id");
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL');
    }
  }

  /* Taken from https://paulund.co.uk/php-delete-directory-and-files-in-directory */
  public function deleteDirectoryOrFile($target) {
    if(is_dir($target)){
      $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
      foreach( $files as $file ){
        $this->deleteDirectoryOrFile($file);      
      }
      @rmdir( $target );
    } else if (is_file($target)) {
      @unlink( $target );  
    }
  }

  /**
   * Adds a playlist to the database.
   * @param string $userId of the user that created the playlist
   * @param string 'id' of the playlist to be selected.
   * @param bool $subscribed, checking if the selected data is for a user
   * subscription list. 
   * @return array with the playlist information; 'username', 'title', 
   *              'description', 'thumbnail', 'videos' etc.
  */
  public function getPlaylists($userId = null, $playlistId = null, $subscribed = false) {
    if ($userId !== null && $playlistId === null && $subscribed === false) {
      $sql = "SELECT * FROM Playlist WHERE userId=? ";
      $sth = $this->db->prepare($sql);
      $sqlData = array($userId);
    } else if ($userId !== null && $playlistId === null && $subscribed === true) {
      $sql = "SELECT p.* FROM Playlist p INNER JOIN SubscribeTo s 
        ON p.id = s.playlistId WHERE s.userId = ?";
      $sth = $this->db->prepare($sql);
      $sqlData = array($userId);
    } else {
      $sql = "SELECT p.id 'playlistId', p.userId, p.title, p.topic, p.course, 
      p.description 'playlistDescription', p.mime,
      v.id 'videoId', v.userId, v.name, v.description 'videoDescription', 
      v.mime, v.views, v.time, i.rank 
      FROM Playlist p
      LEFT JOIN VideosInPlaylist i ON p.id = i.playlistId 
      LEFT JOIN Video v ON v.id = i.videoId WHERE 
      p.id=? ORDER BY i.rank";
      $sth = $this->db->prepare($sql);
      $sqlData = array($playlistId);
    }

    $sth->execute($sqlData);
    $row = $sth->fetchAll();

    $tmp = [];
    if ($row) {
      if ($playlistId !== null) {
        $fileExtension = "png";
        if ($row['mime'] === "image/png") {
          $playlistData['fileExtension'] = "png";
        } else if ($row['mime'] === "image/jpeg") {
          $playlistData['fileExtension'] = "jpeg";
        } else if ($row['mime'] === "image/jpg") {
          $playlistData['fileExtension'] = "jpg";
        }
        $playlistData = array('playlistId' => $row[0]['playlistId'], 
          'title' => $row[0]['title'], 'topic' => $row[0]['topic'],
          'course' => $row[0]['course'], 'description' => $row[0]['playlistDescription'],
          'userId' => $row[0][1], 'mime' => $row[0]['mime'],
          'fileExtension' => $fileExtension);

        $videoData = array();

        foreach ($row as $r) {
          if (isset($r['name'])) {
            $videoFileExtension = "png";
            if ($r['mime'] === "image/png") {
              $videoFileExtension = "png";
            } else if ($r['mime'] === "image/jpeg") {
              $videoFileExtension = "jpeg";
            } else if ($r['mime'] === "image/jpg") {
              $videoFileExtension = "jpg";
            }
            array_push($videoData, array('videoId' => $r['videoId'], 
              'videoName' => $r['name'], 
              'videoDescription' => $r['videoDescription'], 
              'views' => $r['views'], 'time' => $r['time'],
              'userId' => $r['userId'], 'videoRank' => $r['rank'], 
              'fileExtension' => $videoFileExtension 
            ));
          }
        }
        $tmp['status'] = 'OK';
        $tmp['playlist'] = $playlistData;
        $tmp['videos'] = $videoData;
      } else {
        for ($x = 0; $x < count($row); $x++) {
          $fileExtension = "png";
          if ($row[$x]['mime'] === "image/png") {
            $row[$x]['fileExtension'] = "png";
          } else if ($row[$x]['mime'] === "image/jpeg") {
            $row[$x]['fileExtension'] = "jpeg";
          } else if ($row[$x]['mime'] === "image/jpg") {
            $row[$x]['fileExtension'] = "jpg";
          }
        }
        $tmp['status'] = 'OK';
        $tmp['data'] = $row;
      }
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fant ikke spillelisten';
    }
    return $tmp;
  }

  /**
   * @param string $playlistId, id of the playlist to be subscribed to
   * @param string $userId, id of the user subscribing to the playlist.
   * @return array with only element 'status'=='OK' on success.
   *        'status'=='FAIL' on error, on error a message is also returned.
  */
  public function subscribeToPlaylist($playlistId, $userId) {
    $sql = "INSERT INTO SubscribeTo 
      (userId, playlistId) 
      VALUES ( ?, ? )";

    $sth = $this->db->prepare($sql);
    $sqlData = array($userId, $playlistId);
    $sth->execute($sqlData);

    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fikk ikke abonnert deg til spillelisten!';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }
    return $tmp;
  }

  /**
   * Removes the subscription of a user from a playlist.
   * @param string $playlistId, id of the playlist to be unsubscribed from
   * @param string $userId, id of the user unsubscribing.
   * @return array with only element 'status'=='OK' on success.
   *        'status'=='FAIL' on error.
  */
  public function unsubscribeToPlaylist($playlistId, $userId) {
    $sql = 'DELETE FROM SubscribeTo WHERE playlistId=? AND userId=?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($playlistId, $userId));
    if ($sth->rowCount() == 1) {
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL');
    }
  }

  /**
   * Searches through the playlist table, with a searchword.
   * We are using FULLTEXT INDEX on the title, userId, topic,
   * and course columns.
   * @param string word being searched for
   * @return array with status 'OK' on success and 'FAIL' on fail. 
   * With the data package filled with playlists on a 'OK' and empty on 'FAIL'
  */
  public function search($search) {
    $sql = "SELECT * FROM Playlist
      WHERE MATCH (title, course, userId, topic)
      AGAINST (? IN BOOLEAN MODE)";
    $sth = $this->db->prepare($sql);
    $sqlData = array($search);
    $sth->execute($sqlData);

    $row = $sth->fetchAll();

    $tmp = [];
    if ($row) {
      foreach($row as $r) {
        $r['key'] = 'data:image/png;base64,'.base64_encode($r['thumbnail']); 
      }
      $tmp['status'] = 'OK';
      $tmp['data'] = $row;
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['data'] = array();
    }
    return $tmp;
  }

  /* Taken from lecture about file uploading */
  public function scale ($img, $new_width, $new_height) {
    $old_x = imageSX($img);
    $old_y = imageSY($img);
  
    if($old_x > $old_y) {                     // Image is landscape mode
      $thumb_w = $new_width;
      $thumb_h = $old_y*($new_height/$old_x);
    } else if($old_x < $old_y) {              // Image is portrait mode
      $thumb_w = $old_x*($new_width/$old_y);
      $thumb_h = $new_height;
    } if($old_x == $old_y) {                  // Image is square
      $thumb_w = $new_width;
      $thumb_h = $new_height;
    }
  
    if ($thumb_w>$old_x) {                    // Don't scale images up
      $thumb_w = $old_x;
      $thumb_h = $old_y;
    }
  
    $dst_img = ImageCreateTrueColor($thumb_w,$thumb_h);
    imagecopyresampled($dst_img,$img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
  
    ob_start();                         // flush/start buffer
    imagepng($dst_img,NULL,9);          // Write image to buffer
    $scaledImage = ob_get_contents();   // Get contents of buffer
    ob_end_clean();                     // Clear buffer
    return $scaledImage;
  }

}

?>