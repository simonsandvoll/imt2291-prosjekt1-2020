<?php

require_once 'Validator.php';

class Teacher {
	private $twig = null;
	private $video = null;
	private $playlist = null;

	  /**
   * The construct function handles all the GET and POST requests
   * related to the user. And defines the private class variables.
  */
	public function __construct($twig, $video, $playlist) {

		$this->twig = $twig;
		$this->video = $video;
		$this->playlist = $playlist;
		$this->twig->setGlobal('user', $_SESSION['userData']);

		if (isset($_GET['viewVideoForm'])) {
			$this->twig->render('index.html', array('view' => 'videoForm'));

		} else if (isset($_GET['createPlaylist'])) {
			$tmp = $this->video->getVideos($_SESSION['userData']['username']);
			if ($tmp['status'] === 'OK') {
				$this->twig->render('index.html', array('view' => 'playlistForm', 
					'videos' => $tmp['data']));
			} else {
				$this->twig->render('index.html', array('view' => 'playlistForm', 
					'message' => 'Du har ingen videoer ennå!'));
			}
			
		} else if (isset($_GET['showYourVideos'])) {
			$tmp = $this->video->getVideos($_SESSION['userData']['username']);
			if ($tmp['status'] === 'OK') {
				$this->twig->render('index.html', array('videos' => $tmp['data'], 
					'view' => 'showVideos'));
			} else {
				$this->twig->render('index.html', array('message' => 'Du har ingen videoer ennå!', 
					'view' => 'showVideos'));
			}

		} else if (isset($_GET['showYourPlaylists'])) {
			$tmp = $this->playlist->getPlaylists($_SESSION['userData']['username']);
			if ($tmp['status'] === 'OK') {
				$this->twig->render('index.html', array('playlists' => $tmp['data'], 
					'view' => 'showPlaylists'));
			} else {
				$this->twig->render('index.html', array('message' => 'Du har ingen spillelster ennå!', 
					'view' => 'showPlaylists'));
			}

		} else if (isset($_GET['editPlaylist'])) {
			$tmp = $this->playlist->getPlaylists(null, $_GET['editPlaylist']);
			if ($tmp['status'] === 'OK') {
				$otherVideos = $this->video->getOtherVideos($_GET['editPlaylist']);
				if ($otherVideos['status'] === 'OK') {
					$this->twig->render('index.html', array('view' => 'editPlaylist', 
					'playlist' => $tmp['playlist'], 'videos' => $tmp['videos'], 
					'otherVideos' => $otherVideos['data']));
				} else {
					$this->twig->render('index.html', array('view' => 'editPlaylist', 
						'playlist' => $tmp['playlist'], 'videos' => $tmp['videos'], 
						'message' => 'Det finnes ingen andre videoer...'));
				}
			} else {
				 $this->twig->render('error.html', $tmp);
			}

		} else if (isset($_GET['deleteVideo'])) {
			$tmp = $this->video->deleteVideo($_GET['deleteVideo'], $_SESSION['userData']['username']);
			$this->twig->indexOrError($tmp, array('title' => 'Video slettet'));

		} else if (isset($_GET['deletePlaylist'])) {
			$tmp = $this->playlist->deletePlaylist($_GET['deletePlaylist'], $_SESSION['userData']['username']);
			$this->twig->indexOrError($tmp, array('title' => 'Spilleliste slettet'));

		} else if (isset($_GET['editVideo'])) {
			$tmp = $this->video->getVideos(null, $_GET['editVideo']);
			$this->twig->indexOrError($tmp, array('view' => 'editVideo', 'video' => $tmp['data']));

		}
		/* POST REQUESTS */
		else if (isset($_POST['addVideo'])) {
			$this->addVideo($_POST, $_FILES, $_SESSION['userData']['username']);
			
		} else if (isset($_POST['addPlaylist'])) {
			$this->addPlaylist($_POST, $_FILES, $_SESSION['userData']['username']);

		} else if (isset($_POST['savePlaylist'])) {
			$this->savePlaylist($_POST, $_FILES, $_SESSION['userData']['username']);

		} else if (isset($_POST['changeVideo'])) {
			$this->changeVideo($_POST, $_FILES, $_SESSION['userData']['username']);

		} else {
			$this->twig->render('index.html', array());

		}
	}

	/**
	 * @param array $formData is all the data from the post request.
   * @param array $file is the file information from the post request. 
   * @param string $username of the user adding the video.
	 * This data is passed to the video class' createVideo and
	 * prepareDataForInsertion function.
	*/
	private function addVideo($formData, $file, $username) {
		$valid = Validator::validateVideoSubmit($formData, $file);
		if ($valid['status'] === 'OK') {

			$dataToSend = $this->video->prepareDataForInsertion($formData, $file, $username);

			$tmp = $this->video->createVideo($dataToSend);
			$this->twig->indexOrError($tmp, array('title' => 'Video lagt til', "message" => 
				$dataToSend['name'] . " ble lagt til"));

		} else {
			$this->twig->render('index.html', $valid);
		}
	}

	/**
	 * @param array $formData is all the data from the post request.
   * @param array $file is the file information from the post request. 
   * @param string $username of the user adding the Playlist.
	 * This data is passed to the Playlist class' createPlaylist and
	 * prepareDataForInsertion function.
	*/
	private function addPlaylist($formData, $file, $username) {
		$valid = Validator::validatePlaylistSubmit($formData, $file);
		if ($valid['status'] === 'OK') {
						
			if (isset($formData['videosToAdd']) && count($formData['videosToAdd']) >= 1) {
				$videosToAdd = $formData['videosToAdd'];
				$rank = 1;
				for ($x=0; $x<count($videosToAdd); $x++) {
					$id = $videosToAdd[$x];
					array_push($videosToAdd, array('videoId' => $id, 'rank' => $rank));
					$rank = $rank+1;
				}
				$formData['videosToAdd'] = $videosToAdd;
			}
			
			$dataToSend = $this->playlist->prepareDataForInsertion($formData, $file, $username);

			$tmp = $this->playlist->createPlaylist($dataToSend);
			$this->twig->indexOrError($tmp, array('title' => 'Spilleliste lagt til', 'message' => 
				$dataToSend['title'] . " ble lagt til"));

		} else {

			$this->twig->render('index.html', $valid);
		}
	}

	/**
	 * @param array $formData is all the data from the post request.
   * @param array $file is the file information from the post request. 
	 * This data is passed to the Playlist class' updatePlaylist and
	 * prepareDataForInsertion function.
	*/
	private function savePlaylist($formData, $file, $username) {
		$valid = Validator::validatePlaylistSubmit($formData, $file);
		if ($valid['status'] === 'OK') {
			$dataToSend = $this->playlist->prepareDataForInsertion($formData, $file, $username);
			$videosToReorder = array();
			$rank = 1;
			if (isset($formData['videoIds'])) {
				for ($x=0; $x<count($formData['videoIds']); $x++) {
					$id = $formData['videoIds'][$x];
					$rank = $formData['videoOrder'][$x] === '' ? 10 : $formData['videoOrder'][$x];
					array_push($videosToReorder, array('videoId' => $id, 'rank' => $rank));
					$rank = $rank+1;
				}
			}

			if (isset($formData['videosToAdd']) && count($formData['videosToAdd']) >= 1) {
				for ($x=0; $x<count($formData['videosToAdd']); $x++) {
					$id = $formData['videosToAdd'][$x];
					array_push($videosToReorder, array('videoId' => $id, 'rank' => $rank));
					$rank = $rank+1;
				}
			}

			$tmp = $this->playlist->updatePlaylist($dataToSend, $videosToReorder);
			$this->twig->indexOrError($tmp, array('title' => 'Spilleliste endret', 'message' => 
				$dataToSend['title'] . " ble endret"));
			
		} else {
			$this->twig->render('index.html', $valid);
		}
	}

	/**
	 * @param array $formData is all the data from the post request.
   * @param array $file is the file information from the post request. 
	 * This data is passed to the Video class' updateVideo and
	 * prepareDataForInsertion function.
	*/
	private function changeVideo($formData, $file, $username) {
		$valid = Validator::validateVideoSubmit($formData, $file);

		if ($valid['status'] === 'OK') {

			$dataToSend = $this->video->prepareDataForInsertion($formData, $file, $username);
			$tmp = $this->video->updateVideo($dataToSend);
			$this->twig->indexOrError($tmp, array('title' => 'Video endret', "message" => 
				$dataToSend['name'] . " ble endret"));
		
		} else {
			$this->twig->render('index.html', $valid);
		}
	}
}

?>