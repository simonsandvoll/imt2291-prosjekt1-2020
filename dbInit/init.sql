/* MAIN TABLES */
CREATE TABLE `Users`
(
    username VARCHAR (64) NOT NULL,
    fullname VARCHAR(64) NOT NULL,
    email VARCHAR(64) NOT NULL UNIQUE,
    password VARCHAR (255) NOT NULL,
    userType ENUM('admin', 'teacher', 'student') NOT NULL,
    validated BOOLEAN NOT NULL DEFAULT 1,
    PRIMARY KEY (username)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `Video` (
    id VARCHAR(64) NOT NULL,
    userId VARCHAR(64) NOT NULL,
    name VARCHAR(64) NOT NULL,
    description VARCHAR(512),
    views BIGINT NOT NULL DEFAULT 0,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    thumbnail LONGBLOB,
    `mime` VARCHAR(128),
    PRIMARY KEY (id),
    FOREIGN KEY (`userId`) REFERENCES Users(username) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `Playlist` (
    id VARCHAR(64) NOT NULL,
    userId VARCHAR(64) NOT NULL,
    title VARCHAR(64) NOT NULL,
    course VARCHAR(64),
    topic VARCHAR(64),
    description VARCHAR(512),
    thumbnail LONGBLOB,
    `mime` VARCHAR(128),
    PRIMARY KEY(id),
    FOREIGN KEY (userId) REFERENCES Users(username) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `Comment` (
    id VARCHAR(64) NOT NULL,
    userId VARCHAR(64) NOT NULL,
    videoId VARCHAR(64) NOT NULL,
    comment VARCHAR(512) NOT NULL,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (userId) REFERENCES Users(username) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (videoId) REFERENCES Video(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

/* SUB TABLES */
CREATE TABLE SubscribeTo (
    userId VARCHAR(64) NOT NULL,
    playlistId VARCHAR(64) NOT NULL,
    PRIMARY KEY (userId, playlistId),
    FOREIGN KEY (userId) REFERENCES Users(username) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (playlistId) REFERENCES Playlist(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE UserLike (
    userId VARCHAR(64) NOT NULL,
    videoId VARCHAR(64) NOT NULL,
    vote BOOLEAN NOT NULL,
    PRIMARY KEY (userId, videoId),
    FOREIGN KEY (userId) REFERENCES Users(username) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (videoId) REFERENCES Video(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE VideosInPlaylist (
    id INT(11) NOT NULL AUTO_INCREMENT,
    videoId VARCHAR(64) NOT NULL,
    playlistId VARCHAR(64) NOT NULL,
    rank INT(11) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (videoId) REFERENCES Video(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (playlistId) REFERENCES Playlist(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

/* ADDING FULLTEXT INDEXES */
ALTER TABLE Video ADD FULLTEXT KEY ft_Video (name, userId, description);
ALTER TABLE Playlist ADD FULLTEXT KEY ft_Playlist (title, course, userId, topic);

/* INSERT ADMIN USER */
INSERT INTO Users (username, fullname, email, password, userType, validated)
VALUES ('admin', 'admin', 'admin@admin.com', '$2y$10$iBwCNmnr55f2maLFZNZOwelaRG36yf0av0yiidNI0ri3Ij4cVGDqq', 'admin', 1 )