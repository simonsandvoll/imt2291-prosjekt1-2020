<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/User.php';

class UserTest extends \Codeception\Test\Unit {
  /**
   * @var \UnitTester
   */
  protected $tester;
  private $username, $fullname, $email;
  private $password = 'mySecretPassword';
  private $userType = 'student';
  protected $user;
  private $db;

  protected function _before() {
    $db = DB::getDBConnection();
    $this->username = md5('username');
    $this->fullname = md5('fullname');
    $this->email = $this->username.'@test.test';

    $this->userData['username'] = $this->username;
    $this->userData['fullname'] = $this->fullname;
    $this->userData['password'] = $this->password;
    $this->userData['password01'] = $this->password;
    $this->userData['password02'] = $this->password;
    $this->userData['userType'] = $this->userType;
    $this->userData['email'] = $this->username.'@test.test';

    $this->user = new User($db);
  }

  protected function _after() {
    
  }

  /**
   * Checks if it is possible through an object of the User class to add a
   * new user to the user table in the database.
   */ 
  public function testCanCreateUser() {
    $data = $this->user->addUser($this->userData);
    
    $this->assertEquals('OK', $data['status'], 'Failed to create new user.');
    $this->assertTrue(strlen($data['email']) > 1, 'Email not set');
    // Delete user
    $deleted = $this->user->deleteUser($data['email']);
    $this->assertEquals('OK', $deleted['status'], 'User could not be deleted.');
  }

  /**
   * Checks if it possible to log in to a newly created user instance.
   */
  public function testCanDoLogin() {

    $data = $this->user->addUser($this->userData);
    $loginstatus = $this->user->loginUser($this->userData['email'], $this->password);
    $this->assertEquals('OK', $loginstatus['status'], 'Failed to log in.');

    // Check that session id is set correctly
    $this->assertEquals($data['email'], $_SESSION['userData']['email'], 'Bad/missing session email');
    // Delete user
    $deleted = $this->user->deleteUser($data['email']);

    // This login should fail
    $loginstatus = $this->user->loginUser($this->userData['email'], $this->password);
    $this->assertEquals('FAIL', $loginstatus['status'], 'Failed to fail to log in.');
  }

  public function testTeacherValidation() {
    $this->userData['userType'] = 'teacher';
    $data = $this->user->addUser($this->userData);

    $this->assertEquals('OK', $data['status'], 'Failed to create teacher user');

    $this->assertTrue(true, isset($_SESSION['userData']), 'UserData is not set');
    $this->assertEquals(0, $_SESSION['userData']['validated'], 'User is validated');

    $updateUser = $this->user->updateUser($data['email']);
    $this->assertEquals('OK', $updateUser['status'], 'Failed to validated teacher');

    $deleted = $this->user->deleteUser($data['email']);
    $this->assertEquals('OK', $deleted['status'], 'Failed to delete teacher');
  }

  public function testGiveAdminAccess() {
    $data = $this->user->addUser($this->userData);

    $this->assertEquals('OK', $data['status'], 'Failed to create user');

    $this->assertTrue(true, isset($_SESSION['userData']), 'UserData is not set');
    $this->assertEquals('student', $_SESSION['userData']['userType'], 'User is not student');

    $updateUser = $this->user->updateUser($data['email'], false);
    $this->assertEquals('OK', $updateUser['status'], 'Failed to give admin access to user');

    $deleted = $this->user->deleteUser($data['email']);
    $this->assertEquals('OK', $deleted['status'], 'Failed to delete admin');
  }
}
