<?php
require_once '../html/classes/DB.php';

class DBTest extends \Codeception\Test\Unit
{
  /**
   * @var \UnitTester
   */
  protected $tester;

  protected function _before()
  {
  }

  protected function _after()
  {
  }

  public function testCanConnectToDB() {
    $this->assertInstanceOf(
      PDO::class,
      DB::getDBConnection()
    );
  }
}