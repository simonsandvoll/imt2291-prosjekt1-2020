<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/Playlist.php';
require_once '../html/classes/Video.php';
require_once '../html/classes/User.php';

class PlaylistTest extends \Codeception\Test\Unit {
  /**
   * @var \UnitTester
   */
  protected $tester;
  protected $username, $playlistData, $userData, $videoData;
  protected $video;
  private $password = 'mySecretPassword';
  private $db;

  protected function _before() {
    $db = DB::getDBConnection();
    $this->username = md5(date('h-i-s, j-m-y, it is w Day'));
    $this->fullname = md5(date('h-i-s, j-m-y, it is w Day'));

    $this->userData['username'] = $this->username;
    $this->userData['fullname'] = $this->fullname;
    $this->userData['password'] = $this->password;
    $this->userData['password01'] = $this->password;
    $this->userData['password02'] = $this->password;
    $this->userData['email'] = $this->username.'@test.test';
    $this->userData['userType'] = 'teacher';

    $this->playlistData['title'] = 'video title';
    $this->playlistData['description'] = 'video description';
    $this->playlistData['topic'] = 'PHP Programmering';
    $this->playlistData['course'] = 'IMT2291';
    $this->playlistData['thumbnail'] = '';
    $this->playlistData['mime'] = '';

    $this->videoData['name'] = 'video title';
    $this->videoData['description'] = 'video description';
    $this->videoData['thumbnail'] = '';
    $this->videoData['mime'] = '';

    
    $this->user = new User($db);
    $this->playlist = new Playlist($db);
    $this->video = new Video($db);
  }

  protected function _after() {
    
  }

  public function testPlaylistCreate() {
    // Create teacher
    $teacherCreate = $this->user->addUser($this->userData);   
    $this->assertEquals('OK', $teacherCreate['status'], 'Failed to create new teacher.');
    $this->assertTrue(isset($teacherCreate['email']), 'Email is not set');

    // Create video
    $this->videoData['username'] = $this->username;
    $videoCreate = $this->video->createVideo($this->videoData);
    $this->assertEquals('OK', $videoCreate['status'], 'Failed to create new video.');
    $this->assertTrue(isset($videoCreate['id']), 'Id of video is not set');
    $this->playlistData['videosToAdd'] = array(array('videoId' => $videoCreate['id'], 'rank' => 1));

    // Create playlist
    $this->playlistData['username'] = $this->username;
    $playlistCreate = $this->playlist->createPlaylist($this->playlistData);
    $this->assertEquals('OK', $playlistCreate['status'], 'Failed to create new playlist.');
    $this->assertTrue(isset($playlistCreate['id']), 'Id of playlist is not set');

    // Delete video
    $deleted = $this->video->deleteVideo($videoCreate['id'], $this->videoData['username']);
    $this->assertEquals('OK', $deleted['status'], 'Video could not be deleted.');

    // Delete playlist
    $deleted = $this->playlist->deletePlaylist($playlistCreate['id'], $this->playlistData['username']);
    $this->assertEquals('OK', $deleted['status'], 'Playlist could not be deleted.');

    // Delete teacher
    $deleteTeacher = $this->user->deleteUser($teacherCreate['email']);
    $this->assertEquals('OK', $deleteTeacher['status'], 'Teacher could not be deleted.');
  }
}
