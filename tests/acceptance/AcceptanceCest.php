<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/User.php';

class AcceptanceCest {
  protected $user, $userData;

  public function _before(AcceptanceTester $I) {
    $db = DB::getDBConnection();
    $username = md5(date('l jS \of F Y h:i:s A'));
    $fullname = md5(date('l jS h:i:s A \of F Y '));
    $email = $username.'@test.test';
    $password = 'MittHemmeligePassord';

    $this->userData['username'] = $username;
    $this->userData['fullname'] = $fullname;
    $this->userData['password'] = $password;
    $this->userData['password01'] = $password;
    $this->userData['password02'] = $password;
    $this->userData['email'] = $email;

    $this->user = new User($db);
  }

  public function firstPageExists(AcceptanceTester $I) {
    $I->amOnPage('/');
    $I->see('Ikke logget inn');
  }

  public function testCanStudentLogInLogOut(AcceptanceTester $I) {
    $this->userData['userType'] = 'student';
    $email = $this->user->addUser($this->userData)['email'];

    $I->amOnPage('/');
    $I->see('Ikke logget inn');

    $I->amOnPage('/');
    $I->fillField('email', $this->userData['email']);
    $I->fillField('password', $this->userData['password']);
    $I->click('Logg inn');

    $I->see('Student');
    $I->amOnPage('/');
    $I->see('Nye videoer');
    $I->click('Loggut');

    $I->amOnPage('/');
    $I->see('Ikke logget inn');

    $this->user->deleteUser($email);
  }
}